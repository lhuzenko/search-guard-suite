package com.floragunn.searchguard.sgconf;

public interface StaticDefinable {
    
    boolean isStatic();

}
