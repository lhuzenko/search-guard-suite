package com.floragunn.searchguard.sgconf;

public interface Hideable {
    
    boolean isHidden();
    boolean isReserved();

}
