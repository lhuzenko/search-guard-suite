<?xml version="1.0" encoding="UTF-8"?><!-- Copyright 2015-2017 floragunn 
	GmbH Licensed under the Apache License, Version 2.0 (the "License"); you 
	may not use this file except in compliance with the License. You may obtain 
	a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless 
	required by applicable law or agreed to in writing, software distributed 
	under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES 
	OR CONDITIONS OF ANY KIND, either express or implied. See the License for 
	the specific language governing permissions and limitations under the License. -->

<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>com.floragunn</groupId>
		<artifactId>search-guard-suite-parent</artifactId>
		<version>${revision}</version>
	</parent>

	<artifactId>search-guard-suite-security</artifactId>
	<packaging>jar</packaging>
	<name>Search Guard Suite - Security</name>

	<dependencies>
		<!-- Netty 4 transport -->
		<dependency>
			<groupId>org.elasticsearch.plugin</groupId>
			<artifactId>transport-netty4-client</artifactId>
			<version>${elasticsearch.version}</version>
			<exclusions>
				<exclusion>
					<artifactId>jna</artifactId>
					<groupId>org.elasticsearch</groupId>
				</exclusion>
				<exclusion>
					<artifactId>jts</artifactId>
					<groupId>com.vividsolutions</groupId>
				</exclusion>
				<exclusion>
					<artifactId>log4j-api</artifactId>
					<groupId>org.apache.logging.log4j</groupId>
				</exclusion>
				<exclusion>
					<artifactId>spatial4j</artifactId>
					<groupId>org.locationtech.spatial4j</groupId>
				</exclusion>
			</exclusions>
		</dependency>
		
		<!-- Guava -->
		<dependency>
			<groupId>com.google.guava</groupId>
			<artifactId>guava</artifactId>
			<version>${guava.version}</version>
		</dependency>

		<!-- Apache commons cli -->
		<dependency>
			<groupId>commons-cli</groupId>
			<artifactId>commons-cli</artifactId>
			<version>${commons.cli.version}</version>
		</dependency>

		<!-- Bouncycastle -->
		<dependency>
			<groupId>org.bouncycastle</groupId>
			<artifactId>bcpg-jdk15on</artifactId>
			<version>${bc.version}</version>
		</dependency>
		
		<dependency>
			<groupId>org.bouncycastle</groupId>
			<artifactId>bcpkix-jdk15on</artifactId>
			<version>${bc.version}</version>
		</dependency>

		<!-- provided scoped deps -->
		<dependency>
			<groupId>org.elasticsearch</groupId>
			<artifactId>elasticsearch</artifactId>
			<version>${elasticsearch.version}</version>
			<scope>provided</scope>				
		</dependency>
		
		<dependency>
    		<groupId>org.elasticsearch.plugin</groupId>
    		<artifactId>elasticsearch-scripting-painless-spi</artifactId>
    		<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>com.floragunn</groupId>
			<artifactId>search-guard-suite-support</artifactId>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.apache.logging.log4j</groupId>
			<artifactId>log4j-core</artifactId>
			<version>${log4j.version}</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
			<version>${jackson-databind.version}</version>
			<exclusions>
				<exclusion>
					<artifactId>jackson-core</artifactId>
					<groupId>com.fasterxml.jackson.core</groupId>
				</exclusion>
			</exclusions>
		</dependency>
		
		<dependency>
			<groupId>com.jayway.jsonpath</groupId>
			<artifactId>json-path</artifactId>
			<version>2.5.0</version>
			<exclusions>
				<exclusion>
					<groupId>net.minidev</groupId>
					<artifactId>json-smart</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.ow2.asm</groupId>
					<artifactId>asm</artifactId>
				</exclusion>				
			</exclusions>			
		</dependency>

		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-rs-security-jose</artifactId>
			<version>${cxf.version}</version>
			<exclusions>
				<exclusion>
					<groupId>org.glassfish.jaxb</groupId>
					<artifactId>jaxb-runtime</artifactId>
				</exclusion>
				<exclusion>
					<groupId>jakarta.xml.bind</groupId>
					<artifactId>jakarta.xml.bind-api</artifactId>
				</exclusion>
				<exclusion>
					<groupId>javax.activation</groupId>
					<artifactId>activation</artifactId>
				</exclusion>
				<exclusion>
					<groupId>com.sun.activation</groupId>
					<artifactId>javax.activation</artifactId>
				</exclusion>	
				<exclusion>
					<groupId>com.fasterxml.woodstox</groupId>
					<artifactId>woodstox-core</artifactId>
				</exclusion>												
			</exclusions>			
		</dependency>

		<dependency>
			<groupId>com.github.seancfoley</groupId>
			<artifactId>ipaddress</artifactId>
			<version>5.3.3</version>
		</dependency>

		<dependency>
			<groupId>org.elasticsearch.client</groupId>
			<artifactId>elasticsearch-rest-high-level-client</artifactId>
			<version>${elasticsearch.version}</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
    		<groupId>org.apache.httpcomponents</groupId>
    		<artifactId>httpcore</artifactId>
		</dependency>
			
		<dependency>
    		<groupId>org.apache.httpcomponents</groupId>
    		<artifactId>httpasyncclient</artifactId>
		</dependency>

		<!-- Only test scoped dependencies hereafter -->
		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.hamcrest</groupId>
			<artifactId>hamcrest-all</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.elasticsearch.plugin</groupId>
			<artifactId>reindex-client</artifactId>
			<version>${elasticsearch.version}</version>
			<scope>test</scope>
			<exclusions>
				<exclusion>
					<groupId>org.elasticsearch</groupId>
					<artifactId>elasticsearch-ssl-config</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.apache.httpcomponents</groupId>
					<artifactId>httpclient</artifactId>
				</exclusion>

			</exclusions>
		</dependency>

		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpclient</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.apache.httpcomponents</groupId>
					<artifactId>httpcore</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>commons-validator</groupId>
			<artifactId>commons-validator</artifactId>
			<version>1.7</version>
		</dependency>

		<dependency>
			<groupId>org.elasticsearch</groupId>
			<artifactId>elasticsearch-ssl-config</artifactId>
			<version>${elasticsearch.version}</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.elasticsearch.plugin</groupId>
			<artifactId>percolator-client</artifactId>
			<version>${elasticsearch.version}</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.elasticsearch.plugin</groupId>
			<artifactId>lang-mustache-client</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.elasticsearch.plugin</groupId>
			<artifactId>parent-join-client</artifactId>
			<version>${elasticsearch.version}</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.elasticsearch.plugin</groupId>
			<artifactId>aggs-matrix-stats-client</artifactId>
			<version>${elasticsearch.version}</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-core</artifactId>
			<version>${mockito.version}</version>
			<scope>test</scope>
		</dependency>
		
		<dependency>
			<groupId>com.github.stephenc.jcip</groupId>
			<artifactId>jcip-annotations</artifactId>
			<version>1.0-1</version>
			<scope>test</scope>
		</dependency>
	</dependencies>
</project>
